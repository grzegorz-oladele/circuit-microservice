package pl.grzegorz.circuitadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitCreateCommandPublisherPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;
import pl.grzegorz.circuitdomain.event.CircuitCreatedEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class CircuitCreatePublisherAdapter implements CircuitCreateCommandPublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.create.exchange}")
    private String circuitCreateExchange;
    @Value("${spring.rabbitmq.create.routing-key}")
    private String circuitCreateRoutingKey;

    @Override
    public void publish(CircuitAggregate circuitAggregate) {
        var createdEvent = CircuitCreatedEvent.of(circuitAggregate);
        rabbitTemplate.convertAndSend(circuitCreateExchange, circuitCreateRoutingKey, createdEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", createdEvent, circuitCreateExchange);
    }
}