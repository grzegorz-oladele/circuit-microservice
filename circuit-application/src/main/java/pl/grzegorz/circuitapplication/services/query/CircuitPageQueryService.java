package pl.grzegorz.circuitapplication.services.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitPageQueryUseCase;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitPageQueryPort;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

@RequiredArgsConstructor
public class CircuitPageQueryService implements CircuitPageQueryUseCase {

    private final CircuitPageQueryPort circuitPageQueryPort;

    @Override
    public ResultView<CircuitView> findAll(ResourceFilter filter) {
        return circuitPageQueryPort.findAll(filter);
    }
}