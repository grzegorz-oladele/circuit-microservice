package pl.grzegorz.circuitapplication.ports.in.command;

import lombok.Builder;
import pl.grzegorz.circuitdomain.data.CircuitCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface CircuitCreateCommandUseCase {

    UUID create(CircuitCreateCommand command);

    @Builder(setterPrefix = "with")
    record CircuitCreateCommand(
            String name,
            String description,
            String city,
            String postalCode,
            String streetName,
            String streetNumber,
            double length,
            String email,
            String phoneNumber
    ) {

        public CircuitCreateData toCreateData() {
            return CircuitCreateData.builder()
                    .withName(name)
                    .withDescription(description)
                    .withCity(city)
                    .withPostalCode(postalCode)
                    .withStreetName(streetName)
                    .withStreetNumber(streetNumber)
                    .withLength(length)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}