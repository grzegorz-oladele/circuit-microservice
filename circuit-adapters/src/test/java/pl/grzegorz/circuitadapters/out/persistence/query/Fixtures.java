package pl.grzegorz.circuitadapters.out.persistence.query;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.time.LocalDateTime;
import java.util.UUID;

import static pl.grzegorz.circuitadapters.out.persistence.MainFixtures.CIRCUIT_ID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class Fixtures {

    public static CircuitViewEntity circuitViewEntity() {
        return CircuitViewEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("name")
                .withDescription("description")
                .withCity("city")
                .withPostalCode("00-000")
                .withStreetName("street-name")
                .withStreetNumber("0-0")
                .withLength(4.087)
                .withEmail("email@email.pl")
                .withPhoneNumber("123-456-789")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static CircuitView circuitView() {
        return CircuitView.builder()
                .withId(UUID.randomUUID())
                .withName("name")
                .withDescription("description")
                .withCity("city")
                .withPostalCode("00-000")
                .withStreetName("street-name")
                .withStreetNumber("0-0")
                .withLength(4.087)
                .withEmail("email@email.pl")
                .withPhoneNumber("123-456-789")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }
}
