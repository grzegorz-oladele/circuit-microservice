package pl.grzegorz.circuitadapters.exception.handler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.grzegorz.circuitdomain.exception.CircuitAlreadyExistsException;
import pl.grzegorz.circuitdomain.exception.CircuitNotFoundException;

@RestControllerAdvice
@RequiredArgsConstructor
class GlobalExceptionHandler {

    private final HttpServletRequest request;

    @ExceptionHandler(CircuitNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorResponse handleNotFoundException(CircuitNotFoundException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, CircuitAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorResponse handleConflictUseCase(Exception exception) {
        if (exception instanceof CircuitAlreadyExistsException) {
            return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.CONFLICT.value());
        }
        return ErrorResponse
                .getErrorResponse(getDefaultErrorMessage((MethodArgumentNotValidException) exception),
                        getCurrentPath(), HttpStatus.CONFLICT.value());
    }

    private String getDefaultErrorMessage(MethodArgumentNotValidException exception) {
        return exception.getAllErrors().get(0).getDefaultMessage();
    }

    private String getCurrentPath() {
        return request.getServletPath();
    }
}