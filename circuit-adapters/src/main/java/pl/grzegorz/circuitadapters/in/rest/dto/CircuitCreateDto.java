package pl.grzegorz.circuitadapters.in.rest.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase.CircuitCreateCommand;

public record CircuitCreateDto(
        @NotBlank(message = "Name must not be empty")
        String name,
        @NotBlank(message = "Description must not be empty")
        String description,
        @NotBlank(message = "City must not be empty")
        String city,
        @NotBlank(message = "Postal code must not be empty")
        String postalCode,
        @NotBlank(message = "Street name must not be empty")
        String streetName,
        @NotBlank(message = "Street number must not be empty")
        String streetNumber,
        double length,
        @NotBlank(message = "Email must not be empty")
        @Email(message = "Email must not be blank")
        String email,
        @NotBlank(message = "Phone number must not be empty")
        String phoneNumber
) {

    public CircuitCreateCommand toCreateCommand() {
        return CircuitCreateCommand.builder()
                .withName(name)
                .withDescription(description)
                .withCity(city)
                .withPostalCode(postalCode)
                .withStreetName(streetName)
                .withStreetNumber(streetNumber)
                .withLength(length)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}