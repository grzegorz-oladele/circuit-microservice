package pl.grzegorz.circuitadapters.out.persistence.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;
import pl.grzegorz.circuitdomain.exception.CircuitNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class CircuitByIdQueryAdapter implements CircuitByIdQueryPort {

    private final CircuitViewEntityJpaRepository circuitEntityJpaRepository;
    private final CircuitViewMapper circuitMapper;

    @Override
    public CircuitAggregate findById(final String circuitId) {
        return circuitEntityJpaRepository.findById(UUID.fromString(circuitId))
                .map(circuitMapper::toDomain)
                .orElseThrow(() -> new CircuitNotFoundException(String.format("Circuit with id %s not found",
                        circuitId)));
    }
}