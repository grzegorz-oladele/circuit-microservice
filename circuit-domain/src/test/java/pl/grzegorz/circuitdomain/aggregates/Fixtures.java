package pl.grzegorz.circuitdomain.aggregates;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.circuitdomain.data.CircuitCreateData;
import pl.grzegorz.circuitdomain.data.CircuitUpdateData;

import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static CircuitCreateData circuitCreateData() {
        return CircuitCreateData.builder()
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withCity("Poznań")
                .withPostalCode("02-674")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-798")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static CircuitUpdateData circuitUpdateData() {
        return CircuitUpdateData.builder()
                .withName("Tor Poznań")
                .withDescription("The biggest circuit")
                .withCity("Poznań")
                .withPostalCode("02-674")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-798")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }
}