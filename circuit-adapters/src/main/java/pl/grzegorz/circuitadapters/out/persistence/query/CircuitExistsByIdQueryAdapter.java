package pl.grzegorz.circuitadapters.out.persistence.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitExistsByIdQueryPort;
import pl.grzegorz.circuitdomain.exception.CircuitNotFoundException;
import pl.grzegorz.circuitdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class CircuitExistsByIdQueryAdapter implements CircuitExistsByIdQueryPort {

    private final CircuitViewEntityJpaRepository circuitViewEntityJpaRepository;

    @Override
    public void existsById(final String circuitId) {
        if (!circuitViewEntityJpaRepository.existsById(UUID.fromString(circuitId))) {
            throw new CircuitNotFoundException(String.format(
                    ExceptionMessage.CIRCUIT_NOT_FOUND_MESSAGE.getMessage(), circuitId));
        }
    }
}