package pl.grzegorz.circuitapplication.ports.out.circuit.command;


import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

public interface CircuitCreateCommandPort {

    void save(CircuitAggregate circuitAggregate);
}