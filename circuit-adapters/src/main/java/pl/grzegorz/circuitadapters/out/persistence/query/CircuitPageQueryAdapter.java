package pl.grzegorz.circuitadapters.out.persistence.query;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitadapters.helper.CircuitApplicationHelper;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitPageQueryPort;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

@Service
@RequiredArgsConstructor
class CircuitPageQueryAdapter implements CircuitPageQueryPort {

    private final CircuitViewEntityJpaRepository circuitViewEntityJpaRepository;
    private final CircuitApplicationHelper<CircuitViewEntity> lapApplicationHelper;
    private final CircuitViewMapper circuitViewMapper;

    @Override
    public ResultView<CircuitView> findAll(final ResourceFilter filter) {
        Pageable pageRequest = lapApplicationHelper.getPageRequest(filter);
        Page<CircuitViewEntity> circuitPage = circuitViewEntityJpaRepository.findAll(pageRequest);
        return ResultView.<CircuitView>builder()
                .withPageInfo(lapApplicationHelper.toPageInfo(pageRequest, circuitPage))
                .withResults(circuitViewMapper.toViewList(circuitPage.getContent()))
                .build();
    }
}