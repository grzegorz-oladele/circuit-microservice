package pl.grzegorz.circuitadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitUpdateDto;

@RequestMapping("/circuits")
public interface CircuitUpdateApi {

    @PatchMapping("/{circuitId}")
    ResponseEntity<Void> updateCircuitById(
            @PathVariable("circuitId") String circuitId,
            @Valid @RequestBody CircuitUpdateDto circuitDto
    );
}