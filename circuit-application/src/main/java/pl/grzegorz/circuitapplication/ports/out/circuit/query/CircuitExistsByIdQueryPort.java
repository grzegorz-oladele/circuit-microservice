package pl.grzegorz.circuitapplication.ports.out.circuit.query;

public interface CircuitExistsByIdQueryPort {

    void existsById(String circuitId);
}