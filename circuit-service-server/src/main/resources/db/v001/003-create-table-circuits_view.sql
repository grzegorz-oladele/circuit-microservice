CREATE OR REPLACE VIEW circuit_db.circuits.circuits_view AS
SELECT id,
       name,
       description,
       city,
       postal_code,
       street_name,
       street_number,
       length,
       email,
       phone_number,
       created_at,
       modified_at
FROM circuit_db.circuits.circuits