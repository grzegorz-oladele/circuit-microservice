package pl.grzegorz.circuitadapters.out.persistence.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

@Service
@RequiredArgsConstructor
class CircuitUpdateCommandAdapter implements CircuitUpdateCommandPort {

    private final CircuitEntityJpaRepository circuitEntityJpaRepository;
    private final CircuitMapper circuitMapper;

    @Override
    public void update(final String circuitId, final CircuitAggregate circuitAggregate) {
        var circuitEntity = circuitMapper.toEntity(circuitAggregate);
        circuitEntityJpaRepository.save(circuitEntity);
    }
}