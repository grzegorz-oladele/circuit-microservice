package pl.grzegorz.circuitapplication.services.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitCreateCommandPublisherPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.UUID;

@RequiredArgsConstructor
public class CircuitCreateCommandService implements CircuitCreateCommandUseCase {

    private final CircuitCreateCommandPort circuitCreateCommandPort;
    private final CircuitCreateCommandPublisherPort circuitCreateCommandPublisherPort;

    @Override
    public UUID create(CircuitCreateCommand command) {
        var circuitAggregate = CircuitAggregate.create(command.toCreateData());
        circuitCreateCommandPort.save(circuitAggregate);
        circuitCreateCommandPublisherPort.publish(circuitAggregate);
        return circuitAggregate.id();
    }
}