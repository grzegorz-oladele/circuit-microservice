package pl.grzegorz.circuitdomain.data;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder(setterPrefix = "with")
public record CircuitCreateData(
        String name,
        String description,
        String city,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}
