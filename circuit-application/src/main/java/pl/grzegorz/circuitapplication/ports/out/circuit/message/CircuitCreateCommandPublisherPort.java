package pl.grzegorz.circuitapplication.ports.out.circuit.message;

import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

public interface CircuitCreateCommandPublisherPort {

    void publish(CircuitAggregate circuitAggregate);
}