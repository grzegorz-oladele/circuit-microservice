package pl.grzegorz.circuitdomain.exception.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {
    CIRCUIT_NOT_FOUND_MESSAGE("Circuit using id -> [%s] not found"),
    CIRCUIT_ALREADY_EXISTS_MESSAGE("Circuit using name -> [%s] already exists");

    private final String message;
}