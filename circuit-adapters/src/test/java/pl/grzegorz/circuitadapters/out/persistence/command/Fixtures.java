package pl.grzegorz.circuitadapters.out.persistence.command;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import static pl.grzegorz.circuitadapters.out.persistence.MainFixtures.CIRCUIT_ID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class Fixtures {

    public static CircuitEntity circuitEntity() {
        return CircuitEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("name")
                .withDescription("description")
                .withCity("city")
                .withPostalCode("00-000")
                .withStreetName("street-name")
                .withStreetNumber("0-0")
                .withLength(4.087)
                .withEmail("email@email.pl")
                .withPhoneNumber("123-456-789")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }
}