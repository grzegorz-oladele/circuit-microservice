package pl.grzegorz.circuitdomain.event;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.NonNull;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.UUID;

@Builder(access = AccessLevel.PRIVATE, setterPrefix = "with")
public record CircuitUpdatedEvent(
        @NonNull UUID id,
        String name,
        String description,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber

) implements DomainEvent {

    public static CircuitUpdatedEvent of(CircuitAggregate aggregate) {
        return CircuitUpdatedEvent.builder()
                .withId(aggregate.id())
                .withName(aggregate.name())
                .withDescription(aggregate.description())
                .withPostalCode(aggregate.postalCode())
                .withStreetName(aggregate.streetName())
                .withStreetNumber(aggregate.streetNumber())
                .withLength(aggregate.length())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .build();
    }
}