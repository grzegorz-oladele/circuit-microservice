package pl.grzegorz.circuitapplication.vo;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record ConstCircuitUpdateValue(
        UUID id,
        String city,
        LocalDateTime createdAt
) {

    public static ConstCircuitUpdateValue toUpdateValue(CircuitAggregate aggregate) {
        return ConstCircuitUpdateValue.builder()
                .withId(aggregate.id())
                .withCity(aggregate.city())
                .withCreatedAt(aggregate.createdAt())
                .build();
    }
}