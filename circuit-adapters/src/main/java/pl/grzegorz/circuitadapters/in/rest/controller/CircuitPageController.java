package pl.grzegorz.circuitadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.circuitadapters.in.rest.api.CircuitPageApi;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitPageQueryUseCase;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

@RestController
@RequiredArgsConstructor
class CircuitPageController implements CircuitPageApi {

    private final CircuitPageQueryUseCase circuitPageQueryUseCase;

    @Override
    public ResponseEntity<ResultView<CircuitView>> getCircuits(ResourceFilter filter) {
        return ResponseEntity.status(HttpStatus.OK).body(circuitPageQueryUseCase.findAll(filter));
    }
}