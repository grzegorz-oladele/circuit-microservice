# circuit-microservice

## Description

Circuit Service is a vital component of your application, responsible for aggregating motorcycle circuits. It supports
adding and updating circuits, while also providing detailed information about each of them. Through close collaboration
with the rides microservice and event publication, it ensures data continuity and excellent user service quality.

## Technologies Used

- [ ] Spring Boot 3.x.x
- [ ] Hibernate
- [ ] PostgreSQL
- [ ] JUnit
- [ ] Testcontainers
- [ ] RabbitMQ

## Example Usage

- [ ] Endpoint: <span style="color:green">GET</span> /circuits?page=0&size=10
    - Description: Retrieve details of circuits in the application.
    - Response: Returns a list of circuits with their details.
- [ ] Endpoint: <span style="color:green">GET</span> /circuits/`{circuitId}`
    - Description: Retrieve details of a specific circuit based on the ID
    - Parameters: `{bikerId}` - The ID of the circuit.
    - Response: Returns the details of the specified circuit.
- [ ] Endpoint: <span style="color:orange">POST</span>  /circuits
    - Description: Add new circuit to the application
    - Request Body: Provide the necessary circuit information
    - Response: Returns the ID of the newly created circuit
- [ ] Endpoint: <span style="color:purple">PATCH</span> /circuits/`{circuitId}`
    - Description: Update circuit information
    - Parameters: `{circuitId}` - The ID of the circuit
    - Request Body: Provide the necessary circuit information

## Architecture
The project follows Domain-Driven Design (DDD) and event storming principles. It consists of four modules:
- [ ] **Server**: Responsible for application startup, integration tests, and management of environment variables
- [ ] **Domain**: The core module that contains key objects and dedicated methods for working with those objects
- [ ] **Application**: Exposes application use cases
- [ ] **Adapters**: Handles communication with external systems and other microservices within the cluster

## Project status
The circuit-microservice is being actively developed. We are constantly working on adding new features including:
- [ ] implementation of the CQRS pattern
- [ ] addition and implementation of Liquibase library
- [ ] implementation of security using the Spring Security framework
- [ ] increasing test coverage of key application functionalities

# Author
**Grzegorz Oladele**

Thank you for your interest in the motorcycle-microservice project! If you have any questions or feedback, please
feel free to contact us.