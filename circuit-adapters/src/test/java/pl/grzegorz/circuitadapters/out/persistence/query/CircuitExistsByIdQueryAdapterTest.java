package pl.grzegorz.circuitadapters.out.persistence.query;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitdomain.exception.CircuitNotFoundException;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CircuitExistsByIdQueryAdapterTest {

    @InjectMocks
    private CircuitExistsByIdQueryAdapter circuitExistsByIdQueryAdapter;
    @Mock
    private CircuitViewEntityJpaRepository circuitEntityJpaRepository;

    @Test
    void shouldThrowCircuitNotFoundExceptionWhenCircuitWillBeNotExistsInDatabase() {
//        given
        var circuitId = UUID.randomUUID();
        when(circuitEntityJpaRepository.existsById(circuitId)).thenReturn(Boolean.FALSE);
//        when + then
        assertThrows(CircuitNotFoundException.class,
                () -> circuitExistsByIdQueryAdapter.existsById(circuitId.toString()));
    }
}