package pl.grzegorz.circuitadapters.out.persistence.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.List;

@Component
class CircuitViewMapper {

    public CircuitAggregate toDomain(CircuitViewEntity circuit) {
        return CircuitAggregate.builder()
                .withId(circuit.id())
                .withName(circuit.name())
                .withDescription(circuit.description())
                .withCity(circuit.city())
                .withPostalCode(circuit.postalCode())
                .withStreetName(circuit.streetName())
                .withStreetNumber(circuit.streetNumber())
                .withLength(circuit.length())
                .withEmail(circuit.email())
                .withPhoneNumber(circuit.phoneNumber())
                .withCreatedAt(circuit.createdAt())
                .withModifiedAt(circuit.modifiedAt())
                .build();
    }

    public List<CircuitView> toViewList(List<CircuitViewEntity> circuits) {
        return circuits
                .stream()
                .map(this::toView)
                .toList();
    }

    private CircuitView toView(CircuitViewEntity circuit) {
        return CircuitView.builder()
                .withId(circuit.id())
                .withName(circuit.name())
                .withDescription(circuit.description())
                .withCity(circuit.city())
                .withPostalCode(circuit.postalCode())
                .withStreetName(circuit.streetName())
                .withStreetNumber(circuit.streetNumber())
                .withPhoneNumber(circuit.streetNumber())
                .withLength(circuit.length())
                .withEmail(circuit.email())
                .withPhoneNumber(circuit.phoneNumber())
                .withCreatedAt(circuit.createdAt())
                .withModifiedAt(circuit.modifiedAt())
                .build();
    }
}