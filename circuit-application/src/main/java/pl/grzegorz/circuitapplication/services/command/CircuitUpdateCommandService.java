package pl.grzegorz.circuitapplication.services.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitUpdateCommandUseCase;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitUpdateCommandPublisherPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitapplication.vo.ConstCircuitUpdateValue;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

@RequiredArgsConstructor
public class CircuitUpdateCommandService implements CircuitUpdateCommandUseCase {

    private final CircuitUpdateCommandPort circuitUpdateCommandPort;
    private final CircuitByIdQueryPort circuitByIdQueryPort;
    private final CircuitUpdateCommandPublisherPort circuitUpdateCommandPublisherPort;

    @Override
    public void update(String circuitId, CircuitUpdateCommand command) {
        var constCircuitUpdateValue = toConstUpdateValue(circuitId);
        var circuitAggregate = CircuitAggregate.update(command.toUpdateData(constCircuitUpdateValue));
        circuitUpdateCommandPort.update(circuitId, circuitAggregate);
        circuitUpdateCommandPublisherPort.publish(circuitAggregate);
    }

    private ConstCircuitUpdateValue toConstUpdateValue(String circuitId) {
        var circuitAggregate = circuitByIdQueryPort.findById(circuitId);
        return ConstCircuitUpdateValue.toUpdateValue(circuitAggregate);
    }
}