package pl.grzegorz.circuitadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitCreateDto;

import java.util.UUID;

@RequestMapping("/circuits")
public interface CircuitCreateApi {

    @PostMapping
    ResponseEntity<UUID> createCircuit(@Valid @RequestBody CircuitCreateDto circuitDto);
}
