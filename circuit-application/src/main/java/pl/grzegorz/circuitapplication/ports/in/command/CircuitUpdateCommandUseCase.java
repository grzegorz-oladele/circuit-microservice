package pl.grzegorz.circuitapplication.ports.in.command;

import lombok.Builder;
import pl.grzegorz.circuitapplication.vo.ConstCircuitUpdateValue;
import pl.grzegorz.circuitdomain.data.CircuitUpdateData;

import java.time.LocalDateTime;

public interface CircuitUpdateCommandUseCase {

    void update(String circuitId, CircuitUpdateCommand command);

    @Builder(setterPrefix = "with")
    record CircuitUpdateCommand(
            String name,
            String description,
            String postalCode,
            String streetName,
            String streetNumber,
            double length,
            String email,
            String phoneNumber
    ) {

        public CircuitUpdateData toUpdateData(ConstCircuitUpdateValue constUpdateValue) {
            return CircuitUpdateData.builder()
                    .withId(constUpdateValue.id())
                    .withName(name)
                    .withDescription(description)
                    .withCity(constUpdateValue.city())
                    .withPostalCode(postalCode)
                    .withStreetName(streetName)
                    .withStreetNumber(streetNumber)
                    .withLength(length)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withCreatedAt(constUpdateValue.createdAt())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}