package pl.grzegorz.circuitdomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.circuitdomain.data.CircuitCreateData;
import pl.grzegorz.circuitdomain.data.CircuitUpdateData;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.circuitdomain.aggregates.Fixtures.circuitCreateData;
import static pl.grzegorz.circuitdomain.aggregates.Fixtures.circuitUpdateData;

class CircuitAggregateTest {

    private CircuitCreateData circuitCreateData;
    private CircuitUpdateData circuitUpdateData;

    @BeforeEach
    void setup() {
        circuitCreateData = circuitCreateData();
        circuitUpdateData = circuitUpdateData();
    }

    @Test
    void shouldCreateNewCircuitAggregateObject() {
//        given
//        when
        var aggregate = CircuitAggregate.create(circuitCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), instanceOf(UUID.class)),
                () -> assertThat(aggregate.name(), is(circuitCreateData.name())),
                () -> assertThat(aggregate.description(), is(circuitCreateData.description())),
                () -> assertThat(aggregate.city(), is(circuitCreateData.city())),
                () -> assertThat(aggregate.postalCode(), is(circuitCreateData.postalCode())),
                () -> assertThat(aggregate.streetName(), is(circuitCreateData.streetName())),
                () -> assertThat(aggregate.streetNumber(), is(circuitCreateData.streetNumber())),
                () -> assertThat(aggregate.length(), is(circuitCreateData.length())),
                () -> assertThat(aggregate.phoneNumber(), is(circuitCreateData.phoneNumber())),
                () -> assertThat(aggregate.email(), is(circuitCreateData.email()))
        );
    }

    @Test
    void shouldUpdateCircuitAggregateObject() {
//        given
//        when
        var updatedAggregate = CircuitAggregate.update(circuitUpdateData);
//        then
        assertAll(
                () -> assertThat(updatedAggregate.id(), is(circuitUpdateData.id())),
                () -> assertThat(updatedAggregate.name(), is(circuitUpdateData.name())),
                () -> assertThat(updatedAggregate.description(), is(circuitUpdateData.description())),
                () -> assertThat(updatedAggregate.city(), is(circuitUpdateData.city())),
                () -> assertThat(updatedAggregate.postalCode(), is(circuitUpdateData.postalCode())),
                () -> assertThat(updatedAggregate.streetName(), is(circuitUpdateData.streetName())),
                () -> assertThat(updatedAggregate.streetNumber(), is(circuitUpdateData.streetNumber())),
                () -> assertThat(updatedAggregate.length(), is(circuitUpdateData.length())),
                () -> assertThat(updatedAggregate.phoneNumber(), is(circuitUpdateData.phoneNumber())),
                () -> assertThat(updatedAggregate.email(), is(circuitUpdateData.email()))
        );
    }

}