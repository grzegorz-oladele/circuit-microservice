package pl.grzegorz.circuitapplication.services.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitByIdQueryUseCase;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;

@RequiredArgsConstructor
public class CircuitByIdQueryService implements CircuitByIdQueryUseCase {

    private final CircuitByIdQueryPort circuitByIdQueryPort;

    @Override
    public CircuitView findById(String circuitId) {
        var circuitAggregate = circuitByIdQueryPort.findById(circuitId);
        return CircuitView.toView(circuitAggregate);
    }
}