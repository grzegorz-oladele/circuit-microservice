package pl.grzegorz.circuitadapters.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.grzegorz.circuitapplication.resources.PageInfo;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;

import java.util.Objects;

@Component
public class CircuitApplicationHelper<T> {

    public Pageable getPageRequest(ResourceFilter filter) {
        if (Objects.isNull(filter.page()) && Objects.isNull(filter.size())) {
            filter = ResourceFilter.empty();
        }
        return PageRequest.of(filter.page(), filter.size());
    }

    public PageInfo toPageInfo(Pageable pageable, Page<T> circuitPage) {
        return PageInfo.builder()
                .withActualPage(pageable.getPageNumber())
                .withPageSize(pageable.getPageSize())
                .withTotalPages(circuitPage.getTotalPages())
                .withTotalRecordCount(circuitPage.getTotalElements())
                .build();
    }
}