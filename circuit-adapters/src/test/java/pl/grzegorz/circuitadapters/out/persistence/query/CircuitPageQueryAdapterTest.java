package pl.grzegorz.circuitadapters.out.persistence.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.grzegorz.circuitadapters.helper.CircuitApplicationHelper;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.PageInfo;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.circuitadapters.out.persistence.query.Fixtures.circuitView;
import static pl.grzegorz.circuitadapters.out.persistence.query.Fixtures.circuitViewEntity;

@ExtendWith(MockitoExtension.class)
class CircuitPageQueryAdapterTest {

    @InjectMocks
    private CircuitPageQueryAdapter circuitPageQueryAdapter;
    @Mock
    private CircuitViewEntityJpaRepository circuitViewEntityJpaRepository;
    @Mock
    private CircuitApplicationHelper<CircuitViewEntity> lapApplicationHelper;
    @Mock
    private CircuitViewMapper circuitViewMapper;

    private CircuitView circuitView;
    private CircuitViewEntity circuitViewEntity;
    private ResourceFilter filter;
    private Pageable pageRequest;

    @BeforeEach
    void setup() {
        circuitView = circuitView();
        circuitViewEntity = circuitViewEntity();
        filter = new ResourceFilter(null, null);
        pageRequest = PageRequest.of(0, 10);
    }

    @Test
    void shouldReturnPageWithCircuitViewList() {
//        given
        var circuitPage = new PageImpl<>(Collections.singletonList(circuitViewEntity));
        when(lapApplicationHelper.getPageRequest(filter)).thenReturn(pageRequest);
        when(circuitViewEntityJpaRepository.findAll(pageRequest)).thenReturn(circuitPage);
        when(lapApplicationHelper.toPageInfo(pageRequest, circuitPage)).thenReturn(PageInfo.builder()
                .withTotalRecordCount(1)
                .withTotalPages(1)
                .withActualPage(0)
                .withPageSize(10)
                .build());
        when(circuitViewMapper.toViewList(Collections.singletonList(circuitViewEntity)))
                .thenReturn(Collections.singletonList(circuitView));
//        when
        ResultView<CircuitView> circuitResultPage = circuitPageQueryAdapter.findAll(filter);
//        then
        assertAll(
                () -> assertThat(circuitResultPage.pageInfo().actualPage(), is(0)),
                () -> assertThat(circuitResultPage.pageInfo().pageSize(), is(10)),
                () -> assertThat(circuitResultPage.pageInfo().totalPages(), is(1)),
                () -> assertThat(circuitResultPage.pageInfo().totalRecordCount(), is(1L)),
                () -> assertThat(circuitResultPage.results().size(), is(1))
        );
    }
}