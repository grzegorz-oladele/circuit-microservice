package pl.grzegorz.circuitapplication.services.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase.CircuitCreateCommand;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitCreateCommandPublisherPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.circuitapplication.services.Fixtures.createCommand;

@ExtendWith(MockitoExtension.class)
class CircuitCreateCommandServiceTest {

    @InjectMocks
    private CircuitCreateCommandService circuitCreateCommandService;
    @Mock
    private CircuitCreateCommandPort circuitCreateCommandPort;
    @Mock
    private CircuitCreateCommandPublisherPort circuitCreateCommandPublisherPort;

    private CircuitCreateCommand circuitCreateCommand;

    @BeforeEach
    void setup() {
        circuitCreateCommand = createCommand();
    }

    @Test
    void shouldCallSaveMethodFromCircuitCreateCommandPort() {
//        given
//        when
        circuitCreateCommandService.create(circuitCreateCommand);
//        then
        assertAll(
                () -> verify(circuitCreateCommandPort).save(any(CircuitAggregate.class)),
                () -> circuitCreateCommandPublisherPort.publish(any(CircuitAggregate.class))
        );
    }
}