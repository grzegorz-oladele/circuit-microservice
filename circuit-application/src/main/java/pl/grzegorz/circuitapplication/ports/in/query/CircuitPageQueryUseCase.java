package pl.grzegorz.circuitapplication.ports.in.query;

import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

public interface CircuitPageQueryUseCase {

    ResultView<CircuitView> findAll(ResourceFilter filter);
}