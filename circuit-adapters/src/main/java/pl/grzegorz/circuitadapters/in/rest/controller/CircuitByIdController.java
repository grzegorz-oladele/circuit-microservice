package pl.grzegorz.circuitadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.circuitadapters.in.rest.api.CircuitByIdApi;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitByIdQueryUseCase;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;

@RestController
@RequiredArgsConstructor
class CircuitByIdController implements CircuitByIdApi {

    private final CircuitByIdQueryUseCase circuitByIdQueryUseCase;

    @Override
    public ResponseEntity<CircuitView> getCircuitById(String circuitId) {
        return ResponseEntity.status(HttpStatus.OK).body(circuitByIdQueryUseCase.findById(circuitId));
    }
}