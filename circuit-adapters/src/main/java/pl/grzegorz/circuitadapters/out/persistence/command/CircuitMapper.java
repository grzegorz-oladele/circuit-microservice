package pl.grzegorz.circuitadapters.out.persistence.command;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CircuitMapper {

    public CircuitEntity toEntity(CircuitAggregate aggregate) {
        return CircuitEntity.builder()
                .withId(aggregate.id())
                .withName(aggregate.name())
                .withDescription(aggregate.description())
                .withCity(aggregate.city())
                .withPostalCode(aggregate.postalCode())
                .withStreetName(aggregate.streetName())
                .withStreetNumber(aggregate.streetNumber())
                .withLength(aggregate.length())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .build();
    }
}