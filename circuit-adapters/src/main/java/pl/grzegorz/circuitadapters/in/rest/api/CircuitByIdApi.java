package pl.grzegorz.circuitadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;

@RequestMapping("/circuits")
public interface CircuitByIdApi {

    @GetMapping("/{circuitId}")
    ResponseEntity<CircuitView> getCircuitById(@PathVariable("circuitId") String circuitId);
}
