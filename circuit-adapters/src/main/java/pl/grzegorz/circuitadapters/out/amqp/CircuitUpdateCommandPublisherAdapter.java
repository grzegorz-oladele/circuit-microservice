package pl.grzegorz.circuitadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitUpdateCommandPublisherPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;
import pl.grzegorz.circuitdomain.event.CircuitUpdatedEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class CircuitUpdateCommandPublisherAdapter implements CircuitUpdateCommandPublisherPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.update.exchange}")
    private String circuitUpdateExchange;
    @Value("${spring.rabbitmq.update.routing-key}")
    private String circuitUpdateRoutingKey;

    @Override
    public void publish(CircuitAggregate circuitAggregate) {
        var updatedEvent = CircuitUpdatedEvent.of(circuitAggregate);
        rabbitTemplate.convertAndSend(circuitUpdateExchange, circuitUpdateRoutingKey, updatedEvent);
        log.info("Send event -> [{}] to exchange -> [{}]", updatedEvent, circuitUpdateExchange);
    }
}