package pl.grzegorz.circuitadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

@RequestMapping("/circuits")
public interface CircuitPageApi {

    @GetMapping
    ResponseEntity<ResultView<CircuitView>> getCircuits(ResourceFilter filter);
}
