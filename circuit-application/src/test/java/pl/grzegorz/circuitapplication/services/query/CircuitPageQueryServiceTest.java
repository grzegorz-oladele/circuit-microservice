package pl.grzegorz.circuitapplication.services.query;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitPageQueryPort;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CircuitPageQueryServiceTest {

    @InjectMocks
    private CircuitPageQueryService circuitPageQueryService;
    @Mock
    private CircuitPageQueryPort circuitPageQueryPort;

    @Test
    void shouldReturnResultViewWithCircuitViewList() {
//        given
        var filter = new ResourceFilter(null, null);
//        when
        circuitPageQueryService.findAll(filter);
//        then
        verify(circuitPageQueryPort).findAll(filter);
    }
}