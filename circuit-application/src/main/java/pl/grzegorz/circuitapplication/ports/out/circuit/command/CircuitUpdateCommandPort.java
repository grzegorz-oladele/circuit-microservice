package pl.grzegorz.circuitapplication.ports.out.circuit.command;


import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

public interface CircuitUpdateCommandPort {

    void update(String circuitId, CircuitAggregate circuitAggregate);
}