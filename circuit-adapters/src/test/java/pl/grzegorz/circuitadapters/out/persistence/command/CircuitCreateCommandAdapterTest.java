package pl.grzegorz.circuitadapters.out.persistence.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.circuitadapters.out.persistence.MainFixtures.circuitAggregate;
import static pl.grzegorz.circuitadapters.out.persistence.command.Fixtures.circuitEntity;

@ExtendWith(MockitoExtension.class)
class CircuitCreateCommandAdapterTest {

    @InjectMocks
    private CircuitCreateCommandAdapter circuitCreateCommandAdapter;
    @Mock
    private CircuitEntityJpaRepository circuitEntityJpaRepository;
    @Mock
    private CircuitMapper circuitMapper;

    private CircuitAggregate circuitAggregate;
    private CircuitEntity circuitEntity;

    @BeforeEach
    void setup() {
        circuitAggregate = circuitAggregate();
        circuitEntity = circuitEntity();
    }

    @Test
    void shouldCallSaveMethodAndPublishAllMethod() {
//        given
        when(circuitMapper.toEntity(circuitAggregate)).thenReturn(circuitEntity);
//        when
        circuitCreateCommandAdapter.save(circuitAggregate);
//        then
        verify(circuitEntityJpaRepository).save(circuitEntity);
    }
}