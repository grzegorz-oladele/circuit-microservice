package pl.grzegorz.circuitapplication.services.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitUpdateCommandUseCase.CircuitUpdateCommand;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitUpdateCommandPublisherPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.circuitapplication.services.Fixtures.circuitAggregate;
import static pl.grzegorz.circuitapplication.services.Fixtures.circuitUpdateCommand;

@ExtendWith(MockitoExtension.class)
class CircuitUpdateCommandServiceTest {

    @InjectMocks
    private CircuitUpdateCommandService circuitUpdateCommandService;
    @Mock
    private CircuitUpdateCommandPort circuitUpdateCommandPort;
    @Mock
    private CircuitByIdQueryPort circuitByIdQueryPort;
    @Mock
    private CircuitUpdateCommandPublisherPort circuitUpdateCommandPublisherPort;

    private CircuitUpdateCommand circuitUpdateCommand;
    private CircuitAggregate circuitAggregate;

    @BeforeEach
    void setup() {
        circuitUpdateCommand = circuitUpdateCommand();
        circuitAggregate = circuitAggregate();
    }

    @Test
    void shouldCallUpdateMethodFromCircuitByIdQueryPort() {
//        given
        var circuitId = circuitAggregate.id().toString();
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
//        when
        circuitUpdateCommandService.update(circuitId, circuitUpdateCommand);
//        then
        assertAll(
                () -> verify(circuitUpdateCommandPort).update(eq(circuitId), any(CircuitAggregate.class)),
                () -> verify(circuitUpdateCommandPublisherPort).publish(any(CircuitAggregate.class))
        );
    }
}