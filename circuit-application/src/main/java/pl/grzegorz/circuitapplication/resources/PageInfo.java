package pl.grzegorz.circuitapplication.resources;

import lombok.Builder;

@Builder(toBuilder = true, setterPrefix = "with")
public record PageInfo(
        int pageSize,
        int actualPage,
        int totalPages,
        long totalRecordCount
) {

}
