package pl.grzegorz.circuitadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.circuitadapters.in.rest.api.CircuitUpdateApi;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitUpdateDto;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitUpdateCommandUseCase;

@RestController
@RequiredArgsConstructor
class CircuitUpdateController implements CircuitUpdateApi {

    private final CircuitUpdateCommandUseCase circuitUpdateCommandUseCase;

    @Override
    public ResponseEntity<Void> updateCircuitById(String circuitId, CircuitUpdateDto circuitDto) {
        circuitUpdateCommandUseCase.update(circuitId, circuitDto.toUpdateCommand());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}