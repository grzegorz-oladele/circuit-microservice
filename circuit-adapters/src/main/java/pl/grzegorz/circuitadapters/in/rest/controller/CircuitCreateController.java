package pl.grzegorz.circuitadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.circuitadapters.in.rest.api.CircuitCreateApi;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitCreateDto;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
class CircuitCreateController implements CircuitCreateApi {

    private final CircuitCreateCommandUseCase circuitCreateCommandUseCase;

    @Override
    public ResponseEntity<UUID> createCircuit(CircuitCreateDto circuitDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(circuitCreateCommandUseCase.create(circuitDto.toCreateCommand()));
    }
}