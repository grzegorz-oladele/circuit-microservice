package pl.grzegorz.circuitadapters.out.persistence.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CircuitEntityJpaRepository extends JpaRepository<CircuitEntity, UUID> {

    Boolean existsAllByName(String circuitName);
}