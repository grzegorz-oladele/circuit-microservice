package pl.grzegorz.circuitapplication.services;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase.CircuitCreateCommand;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitUpdateCommandUseCase.CircuitUpdateCommand;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static final UUID CIRCUIT_ID = UUID.fromString("98ba7243-999f-4350-810c-cf5c8b013baa");

    public static CircuitAggregate circuitAggregate() {
        return CircuitAggregate.builder()
                .withId(CIRCUIT_ID)
                .withName("Poznan")
                .withDescription("Description of Poznan Circuit")
                .withCity("Poznan")
                .withLength(4.067)
                .build();
    }

    public static CircuitCreateCommand createCommand() {
        return CircuitCreateCommand.builder()
                .withName("Tor Poznań")
                .withDescription("The largest circuit in Poland")
                .withCity("Przeźmierowo")
                .withPhoneNumber("61 853 61 03")
                .withStreetNumber("3")
                .withStreetName("Wyścigowa")
                .withPostalCode("62-081")
                .withLength(4.063)
                .build();
    }

    public static CircuitUpdateCommand circuitUpdateCommand() {
        return CircuitUpdateCommand.builder()
                .withName("Tor Jastrząb")
                .withDescription("Circuit near Radom")
                .withPhoneNumber("61 853 61 03")
                .withStreetNumber("25")
                .withStreetName("Czerwienica")
                .withPostalCode("26-502")
                .withLength(3.5)
                .build();
    }
}
