package pl.grzegorz.circuitapplication.resources;

import lombok.Builder;

import java.util.List;

@Builder(setterPrefix = "with")
public record ResultView<T>(
        PageInfo pageInfo,
        List<T> results
) {
}