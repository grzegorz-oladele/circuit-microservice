package pl.grzegorz.circuitadapters.out.persistence.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface CircuitViewEntityJpaRepository extends JpaRepository<CircuitViewEntity, UUID> {
}