package pl.grzegorz.circuitapplication.ports.out.circuit.query;

import pl.grzegorz.circuitapplication.ports.view.CircuitView;
import pl.grzegorz.circuitapplication.resources.ResourceFilter;
import pl.grzegorz.circuitapplication.resources.ResultView;

public interface CircuitPageQueryPort {

    ResultView<CircuitView> findAll(ResourceFilter filter);
}