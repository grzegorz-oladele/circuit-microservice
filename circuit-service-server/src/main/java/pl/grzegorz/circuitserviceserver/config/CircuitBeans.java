package pl.grzegorz.circuitserviceserver.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitCreateCommandUseCase;
import pl.grzegorz.circuitapplication.ports.in.command.CircuitUpdateCommandUseCase;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitByIdQueryUseCase;
import pl.grzegorz.circuitapplication.ports.in.query.CircuitPageQueryUseCase;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitCreateCommandPublisherPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.message.CircuitUpdateCommandPublisherPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitPageQueryPort;
import pl.grzegorz.circuitapplication.services.command.CircuitCreateCommandService;
import pl.grzegorz.circuitapplication.services.command.CircuitUpdateCommandService;
import pl.grzegorz.circuitapplication.services.query.CircuitByIdQueryService;
import pl.grzegorz.circuitapplication.services.query.CircuitPageQueryService;

@Configuration
class CircuitBeans {

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public CircuitCreateCommandUseCase circuitCreateCommandUseCase(CircuitCreateCommandPort circuitCreateCommandPort,
                                                                   CircuitCreateCommandPublisherPort
                                                                           circuitCreateCommandPublisherPort) {
        return new CircuitCreateCommandService(circuitCreateCommandPort, circuitCreateCommandPublisherPort);
    }

    @Bean
    public CircuitUpdateCommandUseCase circuitUpdateCommandUseCase(CircuitUpdateCommandPort circuitUpdateCommandPort,
                                                                   CircuitByIdQueryPort circuitByIdQueryPort,
                                                                   CircuitUpdateCommandPublisherPort
                                                                           circuitUpdateCommandPublisherPort) {
        return new CircuitUpdateCommandService(circuitUpdateCommandPort, circuitByIdQueryPort,
                circuitUpdateCommandPublisherPort);
    }

    @Bean
    public CircuitByIdQueryUseCase circuitByIdQueryUseCase(CircuitByIdQueryPort circuitByIdQueryPort) {
        return new CircuitByIdQueryService(circuitByIdQueryPort);
    }

    @Bean
    public CircuitPageQueryUseCase circuitPageQueryUseCase(CircuitPageQueryPort circuitPageQueryPort) {
        return new CircuitPageQueryService(circuitPageQueryPort);
    }
}