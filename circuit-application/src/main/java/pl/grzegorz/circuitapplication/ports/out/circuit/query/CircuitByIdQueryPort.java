package pl.grzegorz.circuitapplication.ports.out.circuit.query;


import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

public interface CircuitByIdQueryPort {

    CircuitAggregate findById(String circuitId);
}
