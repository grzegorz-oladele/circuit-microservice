package pl.grzegorz.circuitapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record CircuitView(
        UUID id,
        String name,
        String description,
        String city,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {

    public static CircuitView toView(CircuitAggregate aggregate) {
        return CircuitView.builder()
                .withId(aggregate.id())
                .withName(aggregate.name())
                .withDescription(aggregate.description())
                .withCity(aggregate.city())
                .withPostalCode(aggregate.postalCode())
                .withStreetName(aggregate.streetName())
                .withStreetNumber(aggregate.streetNumber())
                .withLength(aggregate.length())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .build();
    }
}