package pl.grzegorz.circuitserviceserver.in.rest.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitCreateDto;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitUpdateDto;
import pl.grzegorz.circuitadapters.out.persistence.command.CircuitEntity;
import pl.grzegorz.circuitadapters.out.persistence.command.CircuitEntityJpaRepository;
import pl.grzegorz.circuitserviceserver.AbstractIntegrationTest;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.grzegorz.circuitserviceserver.Fixtures.*;

class CircuitControllerTest extends AbstractIntegrationTest {

    private static final String CIRCUIT_PATH = "/circuits";

    @Autowired
    private CircuitEntityJpaRepository circuitEntityJpaRepository;
    private CircuitEntity circuitEntity;
    private CircuitCreateDto circuitCreateDto;
    private CircuitUpdateDto circuitUpdateDto;
    private CircuitCreateDto wrongCircuitDto;

    @BeforeEach
    void setup() {
        circuitEntity = circuitEntityJpaRepository.save(circuitEntity());
        circuitCreateDto = circuitCreateDto();
        circuitUpdateDto = circuitUpdateDto();
        wrongCircuitDto = wrongCircuitCreateDto();
    }

    @AfterEach
    void tearDown() {
        circuitEntityJpaRepository.deleteAll();
    }

    @Test
    void shouldReturnCircuitById() throws Exception {
        //WHEN CIRCUIT EXISTS
        mockMvc.perform(get(String.format(CIRCUIT_PATH + "/%s", CIRCUIT_ID)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(circuitEntity.id().toString())))
                .andExpect(jsonPath("$.name", is(circuitEntity.name())))
                .andExpect(jsonPath("$.description", is(circuitEntity.description())))
                .andExpect(jsonPath("$.city", is(circuitEntity.city())))
                .andExpect(jsonPath("$.streetName", is(circuitEntity.streetName())))
                .andExpect(jsonPath("$.streetNumber", is(circuitEntity.streetNumber())))
                .andExpect(jsonPath("$.length", is(circuitEntity.length())))
                .andExpect(jsonPath("$.phoneNumber", is(circuitEntity.phoneNumber())))
                .andExpect(jsonPath("$.email", is(circuitEntity.email())));
    }

    @Test
    void shouldThrowCircuitNotFoundExceptionWhenCircuitWilBeNotExistsInDatabase() throws Exception {
        var circuitId = UUID.randomUUID();
        mockMvc.perform(get(String.format(CIRCUIT_PATH + "/%s", circuitId)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Circuit with id %s not found", circuitId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldReturnPageWithCircuitList() throws Exception {
        mockMvc.perform(get(CIRCUIT_PATH))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageInfo.pageSize", is(10)))
                .andExpect(jsonPath("$.pageInfo.actualPage", is(0)))
                .andExpect(jsonPath("$.pageInfo.totalPages", is(1)))
                .andExpect(jsonPath("$.pageInfo.totalRecordCount", is(1)))
                .andExpect(jsonPath("$.results", hasSize(1)))
                .andExpect(jsonPath("$.results[0].id", is("d411de0d-a9f2-4844-bd39-437e1a9733e5")))
                .andExpect(jsonPath("$.results[0].name", is("Tor Poznań")))
                .andExpect(jsonPath("$.results[0].description", is("The biggest circuit in Poland")))
                .andExpect(jsonPath("$.results[0].city", is("Poznań")))
                .andExpect(jsonPath("$.results[0].streetName", is("Wyścigowa")))
                .andExpect(jsonPath("$.results[0].streetNumber", is("3")))
                .andExpect(jsonPath("$.results[0].length", is(4.093)))
                .andExpect(jsonPath("$.results[0].email", is("test@email.pl")))
                .andExpect(jsonPath("$.results[0].phoneNumber", is("123456789")));
    }

    @Test
    void shouldCreateNewCircuit() throws Exception {
        var mockDto = mapper.writeValueAsString(circuitCreateDto);
        mockMvc.perform(post(CIRCUIT_PATH)
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
        var circuitEntityList = circuitEntityJpaRepository.findAll();
        assertAll(
                () -> assertThat(circuitEntityList.size(), is(2)),
                () -> assertThat(circuitEntityList.get(1).name(), is("test name dto")),
                () -> assertThat(circuitEntityList.get(1).description(), is("test description dto")),
                () -> assertThat(circuitEntityList.get(1).city(), is("test city dto")),
                () -> assertThat(circuitEntityList.get(1).postalCode(), is("00-000")),
                () -> assertThat(circuitEntityList.get(1).streetName(), is("test street name dto")),
                () -> assertThat(circuitEntityList.get(1).streetNumber(), is("1234")),
                () -> assertThat(circuitEntityList.get(1).length(), is(5.000)),
                () -> assertThat(circuitEntityList.get(1).email(), is("dto@email.pl")),
                () -> assertThat(circuitEntityList.get(1).phoneNumber(), is("203948576"))
        );
    }

    @Test
    void shouldThrowCircuitAlreadyExceptionWhenCircuitWithGivenNameAlreadyExists() throws Exception {
        var mockDto = mapper.writeValueAsString(wrongCircuitDto);
        mockMvc.perform(post(CIRCUIT_PATH)
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.message", is(String.format("Circuit using name -> [%s] already exists", wrongCircuitDto.name()))))
                .andExpect(jsonPath("$.responseCode", is(409)));
    }

    @Test
    void shouldUpdateCircuitById() throws Exception {
//        WHEN CIRCUIT EXISTS
        var circuitId = CIRCUIT_ID;
        var mockDto = mapper.writeValueAsString(circuitUpdateDto);
        mockMvc.perform(patch(String.format(CIRCUIT_PATH + "/%s", circuitId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());
        var circuitEntity = circuitEntityJpaRepository.findById(circuitId)
                .orElse(null);
        assertAll(
                () -> assertThat(circuitEntity.name(), is("updated name")),
                () -> assertThat(circuitEntity.description(), is("updated description")),
                () -> assertThat(circuitEntity.postalCode(), is("11-111")),
                () -> assertThat(circuitEntity.streetName(), is("updated street name")),
                () -> assertThat(circuitEntity.streetNumber(), is("192834bfv")),
                () -> assertThat(circuitEntity.length(), is(3.33)),
                () -> assertThat(circuitEntity.email(), is("updated@email.pl")),
                () -> assertThat(circuitEntity.phoneNumber(), is("000000000"))
        );
    }

    @Test
    void shouldThrowCircuitNotFoundExceptionWhenBeforeUpdateCircuitWillBeNotExistsInDatabase() throws Exception {
        var wrongCircuitId = UUID.randomUUID();
        var mockDto = mapper.writeValueAsString(circuitUpdateDto);
        mockMvc.perform(patch(String.format(CIRCUIT_PATH + "/%s", wrongCircuitId))
                        .content(mockDto)
                        .contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Circuit with id %s not found", wrongCircuitId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }
}