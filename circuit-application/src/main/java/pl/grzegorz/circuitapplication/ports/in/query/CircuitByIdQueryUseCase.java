package pl.grzegorz.circuitapplication.ports.in.query;


import pl.grzegorz.circuitapplication.ports.view.CircuitView;

public interface CircuitByIdQueryUseCase {

    CircuitView findById(String circuitId);
}