package pl.grzegorz.circuitserviceserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Map;
import java.util.Set;

@ActiveProfiles("test")
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
public abstract class AbstractIntegrationTest {

    private static final Network INTEGRATION_TEST_NETWORK = Network.newNetwork();
    private static final String POSTGRES_IMAGE = "postgres:14-alpine";
    private static final String RABBITMQ_IMAGE = "rabbitmq:3.12.0-rc.3-management-alpine";
    private static final String CIRCUIT_TEST_DB = "circuit_db";
    private static final String CIRCUIT_TEST_DB_USER = "circuit";
    private static final String CIRCUIT_TEST_DB_PASSWORD = "circuit";
    private static final String CIRCUIT_DB_ALIAS = "circuit-db";
    private static final String CIRCUIT_PERSIST_EXCHANGE = "circuits.persist";
    private static final String CIRCUIT_UPDATE_EXCHANGE = "circuits.update";
    private static final String RABBITMQ_DEFAULT_V_HOST = "/";
    private static final String RABBITMQ_PERMISSIONS = ".*";
    private static final String EXCHANGE_TYPE = "topic";
    private static final String RABBITMQ_DEFAULT_USER_TAG = "management";
    private static final String RABBIT_USER = "rabbit";
    private static final String RABBIT_PASSWORD = "rabbit";
    private static final String RABBITMQ_ALIAS = "rabbitmq";
    private static final int RABBITMQ_DEFAULT_PORT = 5672;

    private static final PostgreSQLContainer<?> CIRCUIT_SERVICE_POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(CIRCUIT_TEST_DB)
            .withPassword(CIRCUIT_TEST_DB_PASSWORD)
            .withUsername(CIRCUIT_TEST_DB_USER)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(CIRCUIT_DB_ALIAS);

    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer(
            DockerImageName.parse(RABBITMQ_IMAGE))
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(RABBITMQ_ALIAS)
            .withExposedPorts(RABBITMQ_DEFAULT_PORT)
            .withUser(RABBIT_USER, RABBIT_PASSWORD, Set.of(RABBITMQ_DEFAULT_USER_TAG))
            .withPermission(RABBITMQ_DEFAULT_V_HOST, RABBIT_USER, RABBITMQ_PERMISSIONS, RABBITMQ_PERMISSIONS, RABBITMQ_PERMISSIONS)
            .withExchange(RABBITMQ_DEFAULT_V_HOST, CIRCUIT_PERSIST_EXCHANGE, EXCHANGE_TYPE, false, false, true, Map.of())
            .withExchange(RABBITMQ_DEFAULT_V_HOST, CIRCUIT_UPDATE_EXCHANGE, EXCHANGE_TYPE, false, false, true, Map.of());

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper mapper;

    static {
        CIRCUIT_SERVICE_POSTGRE_SQL_CONTAINER.start();
        RABBIT_MQ_CONTAINER.start();
        System.setProperty("spring.rabbitmq.port", RABBIT_MQ_CONTAINER.getMappedPort(RABBITMQ_DEFAULT_PORT).toString());
    }

    @DynamicPropertySource
    public static void postgresContainerConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", CIRCUIT_SERVICE_POSTGRE_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", CIRCUIT_SERVICE_POSTGRE_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", CIRCUIT_SERVICE_POSTGRE_SQL_CONTAINER::getUsername);
    }
}