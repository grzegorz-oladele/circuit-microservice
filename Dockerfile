FROM amazoncorretto:17-alpine
EXPOSE 8400
WORKDIR /app
COPY circuit-service-server/src/main/resources/db /app
COPY circuit-service-server/target/circuit-service-server-0.0.1-SNAPSHOT.jar /app/circuit-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "circuit-service-server-0.0.1-SNAPSHOT.jar"]