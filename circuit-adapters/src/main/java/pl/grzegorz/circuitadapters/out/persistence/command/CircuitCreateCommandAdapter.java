package pl.grzegorz.circuitadapters.out.persistence.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.circuitapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;
import pl.grzegorz.circuitdomain.exception.CircuitAlreadyExistsException;
import pl.grzegorz.circuitdomain.exception.messages.ExceptionMessage;

@Service
@RequiredArgsConstructor
class CircuitCreateCommandAdapter implements CircuitCreateCommandPort {

    private final CircuitEntityJpaRepository circuitEntityJpaRepository;
    private final CircuitMapper circuitMapper;

    @Override
    public void save(final CircuitAggregate circuitAggregate) {
        if (Boolean.TRUE.equals(circuitEntityJpaRepository.existsAllByName(circuitAggregate.name()))) {
            throw new CircuitAlreadyExistsException(
                    String.format(ExceptionMessage.CIRCUIT_ALREADY_EXISTS_MESSAGE.getMessage(), circuitAggregate.name())
            );
        }
        var circuitEntity = circuitMapper.toEntity(circuitAggregate);
        circuitEntityJpaRepository.save(circuitEntity);
    }
}