package pl.grzegorz.circuitserviceserver;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitCreateDto;
import pl.grzegorz.circuitadapters.in.rest.dto.CircuitUpdateDto;
import pl.grzegorz.circuitadapters.out.persistence.command.CircuitEntity;

import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static final UUID CIRCUIT_ID = UUID.fromString("d411de0d-a9f2-4844-bd39-437e1a9733e5");

    public static CircuitEntity circuitEntity() {
        return CircuitEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withCity("Poznań")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withLength(4.093)
                .withPhoneNumber("123456789")
                .withEmail("test@email.pl")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static CircuitCreateDto circuitCreateDto() {
        return new CircuitCreateDto(
                "test name dto",
                "test description dto",
                "test city dto",
                "00-000",
                "test street name dto",
                "1234",
                5.000,
                "dto@email.pl",
                "203948576"
        );
    }

    public static CircuitCreateDto wrongCircuitCreateDto() {
        return new CircuitCreateDto(
                "Tor Poznań",
                "test description dto",
                "test city dto",
                "00-000",
                "test street name dto",
                "1234",
                5.000,
                "dto@email.pl",
                "203948576"
        );
    }

    public static CircuitUpdateDto circuitUpdateDto() {
        return new CircuitUpdateDto(
                "updated name",
                "updated description",
                "11-111",
                "updated street name",
                "192834bfv",
                3.33,
                "updated@email.pl",
                "000000000"
        );
    }
}
