package pl.grzegorz.circuitdomain.event;

import java.util.UUID;

public interface DomainEvent {

    UUID id();
}