package pl.grzegorz.circuitadapters.out.persistence;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MainFixtures {

    public static final UUID CIRCUIT_ID = UUID.fromString("98ba7243-999f-4350-810c-cf5c8b013baa");

    public static CircuitAggregate circuitAggregate() {
        return CircuitAggregate.builder()
                .withId(CIRCUIT_ID)
                .withName("name")
                .withDescription("description")
                .withCity("city")
                .withPostalCode("00-000")
                .withStreetName("street-name")
                .withStreetNumber("0-0")
                .withLength(4.087)
                .withEmail("email@email.pl")
                .withPhoneNumber("123-456-789")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }
}
