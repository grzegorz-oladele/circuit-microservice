package pl.grzegorz.circuitadapters.out.persistence.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.circuitadapters.out.persistence.MainFixtures.CIRCUIT_ID;
import static pl.grzegorz.circuitadapters.out.persistence.MainFixtures.circuitAggregate;
import static pl.grzegorz.circuitadapters.out.persistence.query.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class CircuitByIdQueryAdapterTest {

    @InjectMocks
    private CircuitByIdQueryAdapter circuitByIdQueryAdapter;
    @Mock
    private CircuitViewEntityJpaRepository circuitViewEntityJpaRepository;
    @Mock
    private CircuitViewMapper circuitViewMapper;

    private CircuitAggregate circuitAggregate;
    private CircuitViewEntity circuitViewEntity;

    @BeforeEach
    void setup() {
        circuitAggregate = circuitAggregate();
        circuitViewEntity = circuitViewEntity();
    }

    @Test
    void shouldReturnCircuitAggregateWhenCircuitWillBeExistsInTheDatabase() {
//        given
        var circuitId = CIRCUIT_ID;
        when(circuitViewEntityJpaRepository.findById(circuitId)).thenReturn(Optional.of(circuitViewEntity));
        when(circuitViewMapper.toDomain(circuitViewEntity)).thenReturn(circuitAggregate);
//        when
        CircuitAggregate circuit = circuitByIdQueryAdapter.findById(circuitId.toString());
//        then
        assertAll(
                () -> assertThat(circuit.id(), is(circuitId)),
                () -> assertThat(circuit.name(), is("name")),
                () -> assertThat(circuit.description(), is("description")),
                () -> assertThat(circuit.city(), is("city")),
                () -> assertThat(circuit.postalCode(), is("00-000")),
                () -> assertThat(circuit.streetName(), is("street-name")),
                () -> assertThat(circuit.streetNumber(), is("0-0")),
                () -> assertThat(circuit.length(), is(4.087)),
                () -> assertThat(circuit.email(), is("email@email.pl")),
                () -> assertThat(circuit.phoneNumber(), is("123-456-789"))
        );
    }
}