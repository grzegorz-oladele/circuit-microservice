package pl.grzegorz.circuitapplication.services.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.circuitapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.circuitapplication.services.Fixtures.CIRCUIT_ID;
import static pl.grzegorz.circuitapplication.services.Fixtures.circuitAggregate;

@ExtendWith(MockitoExtension.class)
class CircuitByIdQueryServiceTest {

    @InjectMocks
    private CircuitByIdQueryService circuitByIdQueryService;
    @Mock
    private CircuitByIdQueryPort circuitByIdQueryPort;

    private CircuitAggregate circuitAggregate;

    @BeforeEach
    void setup() {
        circuitAggregate = circuitAggregate();
    }

    @Test
    void shouldReturnCircuitView() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
//        when
        var circuitView = circuitByIdQueryService.findById(circuitId);
//        then
        assertAll(
                () -> assertThat(circuitView.id(), is(circuitAggregate.id())),
                () -> assertThat(circuitView.name(), is(circuitAggregate.name())),
                () -> assertThat(circuitView.description(), is(circuitAggregate.description())),
                () -> assertThat(circuitView.city(), is(circuitAggregate.city())),
                () -> assertThat(circuitView.phoneNumber(), is(circuitAggregate.phoneNumber())),
                () -> assertThat(circuitView.streetName(), is(circuitAggregate.streetName())),
                () -> assertThat(circuitView.streetNumber(), is(circuitAggregate.streetNumber())),
                () -> assertThat(circuitView.postalCode(), is(circuitAggregate.postalCode())),
                () -> assertThat(circuitView.email(), is(circuitAggregate.email())),
                () -> assertThat(circuitView.length(), is(circuitAggregate.length()))
        );
    }
}