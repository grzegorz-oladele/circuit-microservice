package pl.grzegorz.circuitdomain.event;

import lombok.Builder;
import lombok.NonNull;
import pl.grzegorz.circuitdomain.aggregates.CircuitAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record CircuitCreatedEvent(
        @NonNull UUID id,
        String name,
        String description,
        String city,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber
) implements DomainEvent {

    public static CircuitCreatedEvent of(CircuitAggregate aggregate) {
        return CircuitCreatedEvent.builder()
                .withId(aggregate.id())
                .withName(aggregate.name())
                .withDescription(aggregate.description())
                .withCity(aggregate.city())
                .withPostalCode(aggregate.postalCode())
                .withStreetName(aggregate.streetName())
                .withStreetNumber(aggregate.streetNumber())
                .withLength(aggregate.length())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .build();
    }
}